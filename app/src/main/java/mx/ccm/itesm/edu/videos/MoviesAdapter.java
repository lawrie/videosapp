package mx.ccm.itesm.edu.videos;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by L00960401 on 9/22/16.
 */
public class MoviesAdapter extends ArrayAdapter<Movie> {
    public MoviesAdapter(Context ct,int layout, ArrayList<Movie> movies){
        super(ct, layout, movies);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Movie m = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.movie_layout,
                            parent, false);
        }
        TextView textView = (TextView)
                convertView.findViewById(R.id.textView);
        TextView textView02 = (TextView)
                convertView.findViewById(R.id.textView2);
        textView.setText(m.id_video);
        textView02.setText(m.movieName);
        return convertView;
    }
}
