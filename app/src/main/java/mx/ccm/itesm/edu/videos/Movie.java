package mx.ccm.itesm.edu.videos;

import java.io.Serializable;

/**
 * Created by L00960401 on 9/22/16.
 */
public class Movie implements Serializable{
    public String movieName,  id_video;
    public Movie(String movieName, String id_video){
        this.movieName = movieName;
        this.id_video = id_video;
    }
}
