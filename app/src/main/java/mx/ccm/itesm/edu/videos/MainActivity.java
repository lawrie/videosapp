package mx.ccm.itesm.edu.videos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;

public class MainActivity extends YouTubeBaseActivity
        implements YouTubePlayer.OnInitializedListener {

    private String API_KEY
            = "AIzaSyASX2IaagyyJI9s3pfLGWneBNO0DJ67EvA";
    private String id_youtube = "gIDeOYSmQ1w";
    private YouTubePlayerFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragment  = (YouTubePlayerFragment)
                getFragmentManager()
                        .findFragmentById(R.id.fragment);
        Movie m = (Movie) getIntent()
                .getSerializableExtra("movie");
        id_youtube = m.id_video;
        fragment.initialize(API_KEY, this);
    }

    @Override
    public void onInitializationSuccess(
            YouTubePlayer.Provider provider,
            YouTubePlayer youTubePlayer, boolean b) {

        if(!b){
            youTubePlayer.cueVideo(id_youtube);
            youTubePlayer.play();
            youTubePlayer.setFullscreen(true);
        }else {
            youTubePlayer.play();
        }

    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }
}
