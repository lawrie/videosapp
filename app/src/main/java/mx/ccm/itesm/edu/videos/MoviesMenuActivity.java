package mx.ccm.itesm.edu.videos;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MoviesMenuActivity
        extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayList<Movie> movies = new ArrayList<Movie>();
        /*movies.add(new Movie("JEDI", "gIDeOYSmQ1w"));
        movies.add(new Movie("Beatles Sinfónico", "vFrjtxd1Bzw"));
        movies.add(new Movie("Jazz Divas", "sMsBgflBhAc"));
        movies.add(new Movie("Guardias of the Galaxy", "ls7MkHgz_mc"));
        movies.add(new Movie("Baby Groot", "3YiIxopZKpY"));*/


        String jsonFile = loadJsonFile("movies.json");
        //         Log.v("json",jsonFile);

        try {

            JSONObject obj = new JSONObject(jsonFile);

            JSONArray locations = obj.getJSONArray("Movies");

            for (int i = 0; i < locations.length(); i++) {
                JSONObject c = locations.getJSONObject(i);


                String movieName = c.getString("movieName");
                String movieId = c.getString("id_video");
                movies.add(new Movie(movieName, movieId));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        MoviesAdapter moviesAdapter = new MoviesAdapter(this,
                R.layout.movie_layout,movies);
        setListAdapter(moviesAdapter);


    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent it = new Intent(this, MainActivity.class);
        Movie m = (Movie) getListAdapter().getItem(position);
        it.putExtra("movie", m);
        startActivity(it);
    }

    public String loadJsonFile(String filename) {
        String json = null;
        try {
            InputStream is = getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

}
